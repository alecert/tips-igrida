# Tips Igrida

Main userdocs [here](https://igrida.gitlabpages.inria.fr/userdocs/).

## 0. Create new access to IGRIDA (New user)
Don't forget to install [OpenSSH](https://en.wikipedia.org/wiki/OpenSSH) first. This package should be available on your favorite distribution.
```sh
ssh-keygen -t rsa
ssh-copy-id -i ~/.ssh/id_rsa.pub ${USER}@sabre
exit
```
You should be able to connect to the front-end node with :
```sh
ssh ${USER}@igrida-frontend.irisa.fr
```
Create your temporary storage (more information at the end of the document) with :
```sh
mkdir /srv/tempdd/${USER}
```

## 1. Log in / Connect to front-end node
```sh
ssh ${USER}@igrida-frontend.irisa.fr
```


## 2. Run containers with [Singularity](https://sylabs.io/) (for native Singularity containers and [Docker](https://www.docker.com/) images)

The complete documentation for Singularity can be found [here](https://sylabs.io/guides/3.2/user-guide/quick_start.html#overview-of-the-singularity-interface).

### 2.1 Build your image with a virtual machine
#### 2.1.1 Start the virtual machine
To start the virtual machine, first you need to configure your environment on the grid.
```sh
oarsub -I -l walltime=0:30:00
```
The “walltime” option is not mandatory, but remember that the default value is only 10 min. With this command, you submit an interactive job on one core. Interactive means that you can run commands directly in the terminal contrary to passive jobs.

Some options if you need specific resources:
```sh
oarsub -I -l core=6,/nodes=2,/gpu_device=1
```
Now, you need to load the plugin required for the virtual machine called gvirt and launch the vm.
```sh
module load spack/gvirt
gvirt start sing --image /srv/soft/gvirt-images/alpine-3.12.1-singularity-x86_64.qcow2
```
#### 2.1.2 Build your image on the vm
Run the following command to build your image but be sure to specify the correct paths for your [Singularity definition file](https://sylabs.io/guides/3.7/user-guide/definition_files.html#definition-files) and the image to be created.
```sh
gvirt exec sing "export SINGULARITY_CACHEDIR=/mnt/srv/tempdd/$USER/singularity; singularity build /mnt/srv/tempdd/$USER/your_image_to_be_created.sif /mnt/home/your_definition_file.def"
gvirt exec sing /sbin/poweroff
exit
```

### 2.2 Run a container
#### 2.2.1 Configure your environment for your container:
```sh
oarsub -I -l walltime=0:30:00
module load spack/singularity
# You need to create a cache directory into tempdd data storage in order to not fill your home directory.
mkdir -p $SINGULARITY_CACHEDIR # execute only once
```

#### 2.2.2 Run a singularity container
##### Example
```sh
singularity run library://sylabsed/examples/lolcow
# INFO:    Downloading library image
#  79.91 MiB / 79.91 MiB [================================================================================================================] 100.00% 21.59 MiB/s 3s
# WARNING: underlay of /etc/localtime required more than 50 (76) bind mounts
#  _____________________
# < Never give an inch! >
#  ---------------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||
```

##### Download an image locally
```sh
cd /srv/tempdd/${USER}/
singularity pull library://sylabsed/examples/lolcow
```

##### Run a Docker container from Docker Hub
```sh
singularity run docker://alpine
```

##### Run Jupyter Notebook on a container from Docker Hub with GPU support
Nvidia support is activated with option `--nv`
```sh
oarsub -l /gpu_device=1,walltime=1 -I
cd /srv/tempdd/${USER}/
module load spack/singularity
singularity exec --nv docker://tensorflow/tensorflow:latest-gpu-jupyter jupyter notebook --ip=$(hostname -I) --allow-root
```

##### Run a locally built container with GPU support
Nvidia support is activated with option `--nv`.
```sh
oarsub -l /gpu_device=1,walltime=1 -I
module load spack/singularity
singularity exec --nv your_image_name.sif jupyter lab --ip=$(hostname -I) --allow-root
```
Project Jupyter recommends to use [Jupyter Lab](https://jupyter.org/) instead of jupyter notebook.

## 3. Submit a passive job (batch)
```sh
oarsub -S ./myscript.sh
```
myscript.sh must have executable rights otherwise :
```sh
chmod a+x ./myscript.sh
```

## Extra information
- There are multiple kinds of storage :
  - Most up-to-date information [here](https://igrida.gitlabpages.inria.fr/userdocs/guide/storage.html#storage).
  - Temporary storage (`/srv/tempdd/${USER}`).
    - Wiped every two months.
    - High performance for concurrent access.
    - The limit is 20T.
  - Long term storage (`/srv/longdd/${USER}`).
    - similar to tempdd with few exceptions :
        - no file deletion even after two months
        - quotas will be set between 20Go and 50Go per user
        - useful for software environments, container images, virtual machine images, ...
    - you have to send an email to the DSI to get access
  - Persistent storage (`${USER}@sabre:/udd/${USER}`).
    - Run this [script](https://igrida.gitlabpages.inria.fr/userdocs/guide/faq.html#how-to-get-my-irisa-account-quota-limits-disk-usage) (`/soft/bin/homequota`) to show your disk usage
    - The limit is 12GB.
    - Low performance for concurrent access.
    - Mount this remote directory via SSH to easily send your local files to the remote node (ex: with `sshfs`, ...).
  ```sh
  # mount the directory
  sshfs ${USER}@sabre:/udd/${USER} /path/to/local/directory
  # to unmount
  fusermount3 -u /path/to/local/directory
  # to copy without mounting the remote directory
  scp -r path/your_file_or_directory ${USER}@sabre:/udd/${USER} # persistent storage
  scp -r path/your_file_or_directory igrida-frontend.remote:/srv/tempdd/${USER}/ # temporary storage
  # to list your files via ssh
  ssh ${USER}@sabre ls -l /udd/${USER} # persistent storage
  ssh igrida-frontend.remote ls -l /srv/tempdd/${USER}/ # temporary storage
  ```

- There are two kinds of jobs :
  - Interactive jobs (12h lifespan)
  - Passive jobs (longer lifespan and can be scheduled)
